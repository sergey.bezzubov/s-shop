export const filterConfig = [
  {
    title: 'Product category',
    config: [
      {name: 'All', value: 'type'},
      {name: 'Converse', value: 'type'},
      {name: 'Sneakers', value: 'type'},
    ]
  },
  {
    title: 'Sale',
    config: [
      { name: 'All', value: 'sale' },
      { name: 'Bestsellers', value: 'sale' },
      { name: 'Sale', value: 'sale' },
    ]
  },
  {
    title: 'Price',
    config: [
      { name: 'Sort by price', value: 'price' },
      { name: 'ASC', value: 'price' },
      { name: 'DESC', value: 'price' },
    ],
  }
]
