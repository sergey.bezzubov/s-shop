import { defineStore } from 'pinia'
import { ref, watch } from 'vue'
import { loadData } from '../api'

export const useProductsStore = defineStore('products', () => {
  let products = ref([])
  let cart = ref([])

  if (localStorage.getItem('cart-list')) {
    const cartData = localStorage.getItem('cart-list');
    cart.value = JSON.parse(cartData)
  }

  watch(
    cart,
    (cartVal) => {
      localStorage.setItem('cart-list', JSON.stringify(cartVal))
    },
    { deep: true }
  )

  async function getList() {
    const res = await loadData()
    products.value = await res.json();
  }

  // function addToCart (item) {
  //   cart.value.push(item)
  // }

  return { products, cart, getList }
})